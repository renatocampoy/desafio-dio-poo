package device;

import funcionalidades.AparelhoTelefonico;
import funcionalidades.NavegadorInternet;
import funcionalidades.ReprodutorMusical;

public class iPhone implements AparelhoTelefonico, NavegadorInternet, ReprodutorMusical {

    @Override
    public void ligar(String numero) {
        System.out.println("Ligando para " + numero);
    }

    @Override
    public void atender() {
        System.out.println("Atendendo chamada");
    }

    @Override
    public void iniciarCorreioVoz() {
        System.out.println("Chamando correio de voz");
    }

    @Override
    public void adicionarNovaAba() {
        System.out.println("Nova aba do navegador");
    }

    @Override
    public void exibirPagina(String pagina) {
        System.out.println("Exibindo pagina http://" + pagina);
    }

    @Override
    public void atualizarPagina() {
        System.out.println("Pagina atualizada - F5");
    }

    @Override
    public void tocar() {
        System.out.println("Tocando musica");
    }

    @Override
    public void pausar() {
        System.out.println("Pausando a musica");
    }

    @Override
    public void selecionarMusica() {
        System.out.println("Album de musica selecionado");
    }
}

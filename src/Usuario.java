import device.iPhone;

public class Usuario extends iPhone {
    public static void main(String[] args) {
        iPhone device = new iPhone();

        // Funções de telefone
        device.ligar("5541999504077");
        device.atender();
        device.iniciarCorreioVoz();

        // Funções de navegador
        device.adicionarNovaAba();
        device.exibirPagina("google.com");
        device.atualizarPagina();

        // Funções de reprodutor de musica
        device.selecionarMusica();
        device.tocar();
        device.pausar();
    }
}